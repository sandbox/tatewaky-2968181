<?php

namespace Drupal\commerce_factura_electronica_cr\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FacturaElectronicaSettingsForm.
 */
class FacturaElectronicaSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_factura_electronica_cr.facturaelectronicasettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'factura_electronica_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // TODO: Add transmitter id and some information for keys and numeric keys such as sucursal.
    $config = $this->config('commerce_factura_electronica_cr.facturaelectronicasettings');

    $form['api_mode'] = array(
      '#type' => 'radios',
      '#title' => $this->t('What API mode do you set?'),
      '#default_value' => $config->get('api_mode'),
      '#options' => array(
        0 => $this
          ->t('Test'),
        1 => $this
          ->t('Prod'),
      ),
    );
    // Test Fieldset group fields.
    $form['api_test'] = array(
      '#type' => 'fieldset',
      '#title' => $this
        ->t('Test'),
    );
    $form['api_test']['api_reception_url_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL API RECEPCION'),
      '#default_value' => $config->get('api_reception_url_test'),
      '#required' => TRUE,
    ];
    $form['api_test']['api_access_url_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAUTH 2.0 ACCESS TOKEN URL'),
      '#default_value' => $config->get('api_access_url_test'),
      '#required' => TRUE,
    ];
    $form['api_test']['api_client_id_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CLIENT ID'),
      '#default_value' => $config->get('api_client_id_test'),
      '#value' => 'api-stage',
      '#required' => TRUE,
    ];
    $form['api_test']['api_username_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('USERNAME'),
      '#default_value' => $config->get('api_username_test'),
      '#required' => TRUE,
    ];
    $form['api_test']['api_password_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PASSWORD'),
      '#default_value' => $config->get('api_password_test'),
      '#required' => TRUE,
    ];
    $form['api_test']['api_grant_type_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GRANT TYPE'),
      '#default_value' => $config->get('grant_type_test'),
      '#value' => 'password',
      '#required' => TRUE,
    ];
    $form['api_test']['api_client_secret_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CLIENT SECRET'),
      '#default_value' => $config->get('api_client_secret_test'),
    ];
    $form['api_test']['api_scope_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SCOPE'),
      '#default_value' => $config->get('api_scope_test'),
    ];


    // Prod Fieldset group fields.
    $form['api_prod'] = array(
      '#type' => 'fieldset',
      '#title' => $this
        ->t('Production'),
    );
    $form['api_prod']['api_reception_url_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL API RECEPCION'),
      '#default_value' => $config->get('api_reception_url_prod'),
      '#required' => TRUE,
    ];
    $form['api_prod']['api_access_url_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAUTH 2.0 ACCESS TOKEN URL'),
      '#default_value' => $config->get('api_access_url_prod'),
      '#required' => TRUE,
    ];
    $form['api_prod']['api_client_id_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CLIENT ID'),
      '#default_value' => $config->get('api_client_id_prod'),
      '#value' => 'api-prod',
      '#required' => TRUE,
    ];
    $form['api_prod']['api_username_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('USERNAME'),
      '#default_value' => $config->get('api_username_prod'),
      '#required' => TRUE,
    ];
    $form['api_prod']['api_password_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PASSWORD'),
      '#default_value' => $config->get('api_password_prod'),
      '#required' => TRUE,
    ];
    $form['api_prod']['api_grant_type_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GRANT TYPE'),
      '#default_value' => $config->get('grant_type_prod'),
      '#required' => TRUE,
      '#value' => 'password',
    ];
    $form['api_prod']['api_client_secret_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CLIENT SECRET'),
      '#default_value' => $config->get('api_client_secret_prod'),
    ];
    $form['api_prod']['api_scope_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SCOPE'),
      '#default_value' => $config->get('api_scope_prod'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('commerce_factura_electronica_cr.facturaelectronicasettings')
      // Test Values.
      ->set('api_mode', $form_state->getValue('api_mode'))
      ->set('api_reception_url_test', $form_state->getValue('api_reception_url_test'))
      ->set('api_access_url_test', $form_state->getValue('api_access_url_test'))
      ->set('api_client_id_test', $form_state->getValue('api_client_id_test'))
      ->set('api_client_secret_test', $form_state->getValue('api_client_secret_test'))
      ->set('api_scope_test', $form_state->getValue('api_scope_test'))
      ->set('api_username_test', $form_state->getValue('api_username_test'))
      ->set('api_password_test', $form_state->getValue('api_password_test'))
      ->set('grant_type_test', $form_state->getValue('grant_type_test'))
      // Prod values.
      ->set('api_reception_url_prod', $form_state->getValue('api_reception_url_prod'))
      ->set('api_access_url_prod', $form_state->getValue('api_access_url_prod'))
      ->set('api_client_id_prod', $form_state->getValue('api_client_id_prod'))
      ->set('api_client_secret_prod', $form_state->getValue('api_client_secret_prod'))
      ->set('api_scope_prod', $form_state->getValue('api_scope_prod'))
      ->set('api_username_prod', $form_state->getValue('api_username_prod'))
      ->set('api_password_prod', $form_state->getValue('api_password_prod'))
      ->set('grant_type_prod', $form_state->getValue('grant_type_prod'))
      ->save();
  }

}
