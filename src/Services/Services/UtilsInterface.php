<?php

namespace Drupal\commerce_factura_electronica_cr;

/**
 * Interface UtilsInterface.
 */
interface UtilsInterface {

  /**
   * Generate access token from API.
   *
   * @return array
   *    Generated access token object.
   */
  public function generateAccessToken();

  /**
   * Generate consecutive number for key.
   *
   * @return array
   *      Generated consecutive number.
   */
  public function generateConsecutiveNumber();

  /**
   * Generate numeric key.
   *
   * @return array
   *      Generated numeric key.
   */
  public function generateNumerickey($user_transmitter);

  /**
   * Generate digital firm hash.
   *
   * @return string
   *   Generated firm hash.
   */
  public function generateFirm();

  /**
   * Prepare all data to send.
   *
   * @return object
   *   All data formatted to be sent.
   */
  public function prepareDataSend();

}
