<?php

namespace Drupal\commerce_factura_electronica_cr;

use Drupal\Core\Config\ConfigFactory;

/**
 * Class Utils.
 */
class Utils implements UtilsInterface {

  /**
   * Configuration service.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Utils constructor.
   *
   * @param ConfigFactory $config_factory
   *   Configuration service.
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->config = $config_factory->get('commerce_factura_electronica_cr.facturaelectronicasettings');
  }

  /**
   * {@inheritdoc}
   */
  public function generateAccessToken() {
    // Get API mode test or prod.
    $mode = ($this->config->get('api_mode') == 1 ? 'prod' : 'test');
    // Set all parameters for the api token request.
    $api_access_url = $this->config->get('api_access_url_' . $mode);
    $api_client_id = $this->config->get('api_client_id_' . $mode);
    $api_username = $this->config->get('api_username_' . $mode);
    $api_password = $this->config->get('api_password_' . $mode);
    $api_grant_type = $this->config->get('api_grant_type_' . $mode);
    // Consume API.
    $response = \Drupal::httpClient()->post($api_access_url, [
      'verify' => TRUE,
      'form_params' => [
        'grant_type' => $api_grant_type,
        'client_id' => $api_client_id,
        'client_secret' => '',
        'scope' => '',
        'username' => $api_username,
        'password' => $api_password,
      ],
      'headers' => [
        'Content-type' => 'application/x-www-form-urlencoded',
      ],
    ])->getBody()->getContents();

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function generateConsecutiveNumber() {
    // TODO: Implement generateConsecutiveNumber() method.
    $number = NULL;
    return $number;
  }

  /**
   * {@inheritdoc}
   */
  public function generateNumerickey($user_transmitter) {
    $numerickey = NULL;
    $country_code = '506';
    if ($date = $this->prepareDate('now')) {
      // TODO: add more info about transmitter user and security code and more
      // country code + day + month + year + transmitter id + consecutive number (function) + 1 + security code.
      // Generate Numeric Key.
      $numerickey = $country_code . $date->day . $date->month . $date->year . $user_transmitter->id;
    }

    return $numerickey;
  }

  /**
   * Set date format on day month and Year.
   *
   * @param string $date_time
   *    Date time.
   *
   * @return \stdClass
   *    Date object.
   */
  private function prepareDate($date_time) {
    $date = new \stdClass();
    // Set the start date with the timezone defined.
    $date->today = new \DateTime($date_time, new \DateTimeZone(drupal_get_user_timezone));
    // Set the custom format.
    $date->day = $date->today->format('d');
    $date->month = $date->today->format('M');
    $date->year = $date->today->format('Y');
    return $date;
  }

  /**
   * {@inheritdoc}
   */
  public function generateFirm() {
    // TODO: Implement generateFirm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function prepareDataSend() {
    // TODO: Implement prepareDataSend() method.
  }

}
